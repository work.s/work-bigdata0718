package cn.atuyang.work;

import org.apache.hadoop.ipc.VersionedProtocol;

/**
 * @author aTuYang
 */
public interface MyInterface extends VersionedProtocol {
    long versionID = 1L;
    int add(int number, int number2);
    String findName(String studentId);
}
