package cn.atuyang.work;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.ipc.RPC;

import java.io.IOException;
import java.net.InetSocketAddress;

/**
 * @author aTuYang
 */
public class RpcClient {
    public static void main(String[] args) {
        try {
            final MyInterface proxy = RPC.getProxy(MyInterface.class, 1L, new InetSocketAddress("127.0.0.1", 12345), new Configuration());
            String res = proxy.findName(args[0]);
            System.out.println(res);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
