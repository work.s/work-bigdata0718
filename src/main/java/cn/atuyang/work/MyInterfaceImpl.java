package cn.atuyang.work;

import org.apache.hadoop.ipc.ProtocolSignature;

import java.io.IOException;

/**
 * @author aTuYang
 */
public class MyInterfaceImpl implements MyInterface {

    @Override
    public int add(int number, int number2) {
        System.out.println("number1 = " + number + ", number2 = " + number2);
        return number + number2;
    }

    @Override
    public String findName(String studentId) {
        if ("20210123456789".equals(studentId)) {
            return "心心";
        } else if ("G20210735010232".equals(studentId)) {
            return "杨训垚";
        }
        return null;
    }

    @Override
    public long getProtocolVersion(String s, long l) throws IOException {
        return MyInterface.versionID;
    }

    @Override
    public ProtocolSignature getProtocolSignature(String s, long l, int i) throws IOException {
        return null;
    }
}
